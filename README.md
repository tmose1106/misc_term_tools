# misc-term-tools

_A Group of Miscellanous Terminal Tools_

## Tools

This repo includes a large assortment of command line scripts which can
act as useful tools. A complete list of the include scripts is below.

### create-icons

Generates icons (16x16, 32x32, 48x48, 64x64, 128x182) from source files
passed as arguments using ImageMagick and gtk-update-icon-cache. Files will
be added to their respective folder in  _/usr/share/icons/hicolor_.

### record-screen

Allows the user to select an area to record and then records it using FFmpeg.

## Licensing

Copyright © 2016 Ted Moseley. Free use of this software is granted under the
terms of the [MIT Open Source License](https://opensource.org/licenses/MIT).
