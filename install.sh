# Install scripts into the user's local bin directory

INSTALL_ROOT="$HOME/.local/bin"

for file_path in src/*
do
  cp -v "$file_path" "$INSTALL_ROOT"
done
